from django import conf
from django.conf.urls import static
from django.contrib import admin
from django import urls

urlpatterns = [
    urls.path('', urls.include('app.urls')),
    urls.path('admin/', admin.site.urls),
]
urlpatterns += static.static(conf.settings.STATIC_URL, document_root=conf.settings.STATIC_ROOT)
