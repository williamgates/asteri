from django.contrib import admin

from app import models


@admin.register(models.PlayerData)
class PlayerDataAdmin(admin.ModelAdmin):
    fields = ('timestamp', 'player_name', 'last_stage', 'stage_name')
    list_display = ('player_name', 'last_stage', 'stage_name', 'timestamp')
    list_display_links = ('player_name',)
    readonly_fields = ('timestamp',)
