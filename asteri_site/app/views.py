import json

from django import http, views
from django.utils import decorators
from django.views import generic
from django.views.decorators import csrf

from app import constants, models


class HomeView(generic.TemplateView):
    template_name = 'app/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['recent_players'] = models.PlayerData.objects.all().order_by('-timestamp')[:10]
        context['top_5_players'] = models.PlayerData.objects.all().order_by('-last_stage')[:5]
        return context


class PingView(views.View):
    def get(self, request):
        return http.HttpResponse('200 OK')


@decorators.method_decorator(csrf.csrf_exempt, name='dispatch')
class PostPlayerDataView(views.View):
    def post(self, request):
        if request.META.get('HTTP_AUTHORIZATION') != constants.AUTHORIZATION:
            return http.HttpResponseForbidden('403 Forbidden')
        try:
            models.PlayerData(**json.loads(request.body)).save()
        except Exception:
            return http.HttpResponseBadRequest('400 Bad Request')
        return http.HttpResponse('200 OK')
