from django import urls

from app import views

app_name = 'app'
urlpatterns = [
    urls.path('', views.HomeView.as_view(), name='home'),
    urls.path('ping/', views.PingView.as_view(), name='ping'),
    urls.path('post-player-data/', views.PostPlayerDataView.as_view(), name='post-player-data'),
]
