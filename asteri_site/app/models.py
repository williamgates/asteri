import uuid

from django.db import models


class PlayerData(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name='ID')
    last_stage = models.IntegerField(verbose_name='Last stage')
    player_name = models.CharField(max_length=50, verbose_name='Player name')
    stage_name = models.CharField(max_length=50, verbose_name='Stage name')
    timestamp = models.DateTimeField(auto_now=True, db_index=True, verbose_name='Timestamp')

    class Meta:
        ordering = ['-timestamp']
        verbose_name = 'Player data'
        verbose_name_plural = 'Player data'

    def __str__(self):
        return self.player_name
