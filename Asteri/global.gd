extends Node

const ASTERI_SITE = "https://asteri-game.herokuapp.com/"
const ASTERI_SITE_POST_PATH = "post-player-data/"
const GRAVITY = Vector2(0, 2400)
const GAME_OVER_SCREEN_PATH = "res://objects/screens/game_over/game_over.tscn"
const MAIN_MENU_SCREEN_PATH = "res://objects/screens/main_menu/main_menu.tscn"
const REQUEST_HEADERS = ["Content-Type: application/json", "Authorization: Basic ($4c#gs3re6ic#0$9su5)itn*q4zov@3i7czk2&a_b3yan0gai"]
const UP = Vector2(0, -1)

var last_stage = 0
var stage_name = "None"
