extends Area2D

export(float) var new_zoom
export(NodePath) var camera_path

var camera = null

func _ready():
	camera = get_node(camera_path)

func _on_ZoomTrigger_body_entered(body):
	if body.get_name() == "Player":
		camera.new_camera_zoom = new_zoom
