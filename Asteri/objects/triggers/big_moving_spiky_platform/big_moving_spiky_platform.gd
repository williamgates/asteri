extends Area2D

export(bool) var move
export(NodePath) var platform_path

var is_enabled = true
var platform = null

func _ready():
	platform = get_node(platform_path)

func _on_BigMovingSpikyPlatform_body_entered(body):
	if (is_enabled) and (body.get_name() == "Player"):
		platform.is_moving = move
		is_enabled = false
