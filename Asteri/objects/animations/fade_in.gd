extends ColorRect

signal animation_finished

func fade_in():
	$AnimationPlayer.play("animation")

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("animation_finished")
