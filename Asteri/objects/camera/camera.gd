extends Camera2D

export(NodePath) var fade_in_path = null
export(NodePath) var target_path = null

var fade_in = null
var new_camera_zoom = self.zoom.x
var target = null

func _on_FadeIn_animation_finished():
	get_tree().change_scene(global.GAME_OVER_SCREEN_PATH)

func _on_Player_died():
	self.new_camera_zoom = 1
	yield(get_tree().create_timer(2.0), "timeout")
	if fade_in:
		fade_in.show()
		fade_in.fade_in()

func _on_Player_teleport():
	self.new_camera_zoom = 1
	yield(get_tree().create_timer(2.0), "timeout")
	if fade_in:
		fade_in.show()
		fade_in.fade_in()

func _physics_process(delta):
	var target_position = target.position
	self.position += (target_position - self.position + Vector2(64, 0)) * 0.5
	var new_zoom = lerp(self.zoom.x, new_camera_zoom, 0.05)
	self.zoom = Vector2(new_zoom, new_zoom)

func _ready():
	fade_in = get_node(fade_in_path)
	target = get_node(target_path)


func _on_ZoomTrigger2_body_entered(body):
	pass # Replace with function body.


func _on_ZoomTrigger_body_entered(body):
	pass # Replace with function body.
