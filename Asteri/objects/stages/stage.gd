extends Node2D

export(int) var stage_number = 0
export(NodePath) var current_stage_status_path = null
export(String) var stage_name = null

var current_stage_status = null

func _ready():
	current_stage_status = get_node(current_stage_status_path).get_node("MarginContainer").get_node("Label")
	current_stage_status.text = "Stage " + str(stage_number) + " - " + stage_name
	global.last_stage = self.stage_number
	global.stage_name = self.stage_name
