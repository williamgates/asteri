extends Area2D

func _on_DashingPoint_body_entered(body):
	if body.get_name() == "Player":
		if self.rotation_degrees < 179:
			body.dash("right")
		else:
			body.dash("left")
