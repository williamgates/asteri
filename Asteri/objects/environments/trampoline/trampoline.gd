extends Area2D

var is_touched = false
var touch_delta = 0

func _on_Trampoline_body_entered(body):
	if body.get_name() == "Player":
		if self.rotation_degrees < 179:
			body.jump(-body.jump_strength * 1.2)
		else:
			body.jump(body.jump_strength * 1.2)
		is_touched = true
		
func _process(delta):
	if is_touched:
		if touch_delta != 8:
			if touch_delta < 4:
				self.scale.x += 0.05
				self.scale.y += 0.05
			else:
				self.scale.x -= 0.05
				self.scale.y -= 0.05
			touch_delta += 1
		else:
			is_touched = false
			touch_delta = 0
