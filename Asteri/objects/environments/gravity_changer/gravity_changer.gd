extends Area2D

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		get_parent().get_node("AudioStreamPlayer").play()
		if get_parent().rotation_degrees < 179:
			body.gravity_vector = -1
			body.get_node("Particles2D").position.y = -body.default_particle_position
		else:
			body.gravity_vector = 1
			body.get_node("Particles2D").position.y = body.default_particle_position
