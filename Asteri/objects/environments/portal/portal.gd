extends Area2D

export(NodePath) var destination_portal_path

var destination_portal = null
var is_enabled = true

func _ready():
	destination_portal = get_node(destination_portal_path)

func _on_Portal_body_entered(body):
	yield(get_tree().create_timer(0.2), "timeout")
	if (is_enabled) and (body.get_name() == "Player"):
		$AudioStreamPlayer.play()
		destination_portal.is_enabled = false
		body.position = destination_portal.position
		yield(get_tree().create_timer(2), "timeout")
		destination_portal.is_enabled = true
