extends Node2D

export(float) var speed

var is_moving = false

func _process(delta):
	if is_moving:
		if self.rotation_degrees < 89:
			self.position += Vector2(0, delta * -speed)
		elif self.rotation_degrees < 179:
			self.position += Vector2(delta * speed, 0)
		elif self.rotation_degrees < 269:
			self.position += Vector2(0, delta * speed)
		elif self.rotation_degrees < 359:
			self.position += Vector2(delta * -speed, 0)
