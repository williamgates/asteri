extends StaticBody2D

export(float) var rotation_interval

var _timer
var is_rotating = false

func _on_Timer_timeout():
	is_rotating = true

func _process(delta):
	if is_rotating:
		var degrees = self.rotation_degrees + delta * 360
		if degrees > 180:
			self.rotation_degrees = 0
			is_rotating = false
		else:
			self.rotation_degrees = degrees

func _ready():
	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_on_Timer_timeout")
	_timer.set_wait_time(rotation_interval)
	_timer.set_one_shot(false)
	_timer.start()
