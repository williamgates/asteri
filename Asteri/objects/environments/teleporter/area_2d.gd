extends Area2D

var is_enabled = true
var scene_path = null

func _on_Area2D_body_entered(body):
	yield(get_tree().create_timer(0.5), "timeout")
	if (is_enabled) and (body.get_name() == "Player"):
		get_parent().get_node("AudioStreamPlayer").play()
		is_enabled = false
		body.teleport()
		yield(get_tree().create_timer(2.0), "timeout")
		get_tree().change_scene(scene_path)
