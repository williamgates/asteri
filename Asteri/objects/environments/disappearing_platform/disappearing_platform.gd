extends StaticBody2D

export(float) var appear_interval
export(float) var disappear_interval
export(float) var start_delay

var _timer

func _on_Timer_timeout():
	self.hide()
	$CollisionPolygon2D.disabled = true
	yield(get_tree().create_timer(disappear_interval), "timeout")
	self.show()
	$CollisionPolygon2D.disabled = false

func _ready():
	yield(get_tree().create_timer(start_delay), "timeout")
	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_on_Timer_timeout")
	_timer.set_wait_time(appear_interval + disappear_interval)
	_timer.set_one_shot(false)
	_timer.start()
	_on_Timer_timeout()
