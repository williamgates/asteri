extends Control

export(NodePath) var back_button_path = null
export(NodePath) var name_input_path = null
export(NodePath) var send_button_path = null
export(NodePath) var send_play_record_status_path = null

var back_button = null
var button_pressed = null
var name_input = null
var scene_path = null
var send_button = null
var send_play_record_status = null

func _on_Back_pressed(button, path):
	back_button.get_node("ButtonSound").play()
	button_pressed = button
	scene_path = path
	$FadeIn.show()
	$FadeIn.fade_in()

func _on_FadeIn_animation_finished():
	get_tree().change_scene(scene_path)

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	if (result != $HTTPRequest.RESULT_SUCCESS) or (response_code != 200):
		send_play_record_status.text = "Failed to send data."
		send_button.disabled = false
	else:
		send_play_record_status.text = "Data sent successfully. You can see your record at " + global.ASTERI_SITE + "."

func _on_Send_pressed(button):
	send_button.get_node("ButtonSound").play()
	send_button.disabled = true
	var data = {"last_stage": global.last_stage, "player_name": name_input.get_text(), "stage_name": global.stage_name}
	var query = JSON.print(data)
	$HTTPRequest.request(global.ASTERI_SITE + global.ASTERI_SITE_POST_PATH, global.REQUEST_HEADERS, false, HTTPClient.METHOD_POST, query)
	send_play_record_status.text = "Sending..."

func _ready():
	back_button = get_node(back_button_path)
	name_input = get_node(name_input_path)
	send_button = get_node(send_button_path)
	send_play_record_status = get_node(send_play_record_status_path).get_node("MarginContainer").get_node("Label")
	back_button.connect("pressed", self, "_on_Back_pressed", [back_button.name, back_button.scene_path])
	send_button.connect("pressed", self, "_on_Send_pressed", [send_button.name])
