extends Control

export(NodePath) var play_button_path = null
export(NodePath) var quit_button_path = null

var button_pressed = null
var play_button = null
var scene_path = null
var quit_button = null

func _on_FadeIn_animation_finished():
	if button_pressed == "Quit":
		get_tree().quit()
	else:
		get_tree().change_scene(scene_path)

func _on_Play_pressed(button, path):
	play_button.get_node("ButtonSound").play()
	button_pressed = button
	scene_path = path
	$FadeIn.show()
	$FadeIn.fade_in()

func _on_Quit_pressed(button):
	quit_button.get_node("ButtonSound").play()
	button_pressed = button
	$FadeIn.show()
	$FadeIn.fade_in()

func _ready():
	play_button = get_node(play_button_path)
	quit_button = get_node(quit_button_path)
	play_button.connect("pressed", self, "_on_Play_pressed", [play_button.name, play_button.scene_path])
	quit_button.connect("pressed", self, "_on_Quit_pressed", [quit_button.name])
