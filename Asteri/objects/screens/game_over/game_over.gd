extends Control

export(NodePath) var main_menu_button_path = null

var button_pressed = null
var main_menu_button = null
var scene_path = null

func _on_MainMenu_pressed(button, path):
	main_menu_button.get_node("ButtonSound").play()
	button_pressed = button
	scene_path = path
	$FadeIn.show()
	$FadeIn.fade_in()

func _on_FadeIn_animation_finished():
	get_tree().change_scene(scene_path)

func _ready():
	main_menu_button = get_node(main_menu_button_path)
	main_menu_button.connect("pressed", self, "_on_MainMenu_pressed", [main_menu_button.name, main_menu_button.scene_path])
