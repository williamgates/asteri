extends KinematicBody2D

export(int) var jump_strength
export(int) var speed

signal died
signal teleport

var dash_counter = 0
var dash_direction = null
var default_particle_position = 0
var gravity_vector = 1
var is_moveable = true
var is_jumping = false
var jump_delta = 0
var jump_direction_vector = 0
var motion = Vector2(0, 0)

func change_gravity():
	gravity_vector *= -1
	$Particles2D.position.y *= -1

func dash(direction):
	dash_counter += 1
	dash_direction = direction
	yield(get_tree().create_timer(1.0), "timeout")
	var counter = dash_counter - 1
	if counter < 0:
		dash_counter = 0
	else:
		dash_counter = counter

func die():
	is_moveable = false
	self.hide()
	$AudioStreamPlayer.play()
	emit_signal("died")

func get_input():
	motion.x = 0
	if is_on_floor() and Input.is_action_pressed("ui_up"):
		jump(gravity_vector * -jump_strength)
	if Input.is_action_pressed("ui_right"):
		motion.x += speed
		if dash_counter > 0:
			if dash_direction == "right":
				motion.x += speed
			else:
				dash_counter = 0
	if Input.is_action_pressed("ui_left"):
		motion.x -= speed
		if dash_counter > 0:
			if dash_direction == "left":
				motion.x -= speed
			else:
				dash_counter = 0

func jump(strength):
	motion.y = strength
	is_jumping = true

func teleport():
	is_moveable = false
	emit_signal("teleport")

func _physics_process(delta):
	get_input()
	motion += (delta * global.GRAVITY * gravity_vector)
	if is_moveable:
		motion = move_and_slide(motion, global.UP * gravity_vector)
	if motion.x == 0:
		dash_counter = 0
	if is_on_floor():
		if is_jumping:
			is_jumping = false
			jump_delta = 0
		else:
			var current_rotate = int(rad2deg(get_node("Sprite").rotation)) % 360
			var current_right_rotate = current_rotate % 90
			var diff = current_rotate - current_right_rotate
			var new_rotation = diff + 90
			if not round(current_rotate) == diff:
				get_node("Sprite").rotation = deg2rad(new_rotation)
	else:
		if is_jumping:
			if jump_delta == 0:
				if motion.x == 0:
					jump_direction_vector = 0
				elif motion.x > 0:
					jump_direction_vector = gravity_vector * 1
				elif motion.x < 0:
					jump_direction_vector = gravity_vector * -1
			if jump_delta != 9:
				get_node("Sprite").rotate(deg2rad(jump_direction_vector * 10))
				jump_delta += 1
			else:
				is_jumping = false
				jump_delta = 0

func _process(delta):
	if motion.x == 0 or !is_on_floor():
		$Particles2D.emitting = false
	if is_on_floor():
		if motion.x > 0:
			$Particles2D.rotation_degrees = 190
			$Particles2D.emitting = true
		elif motion.x < 0:
			$Particles2D.rotation_degrees = -10
			$Particles2D.emitting = true

func _ready():
	default_particle_position = $Particles2D.position.y

	$Particles2D.emitting = false
