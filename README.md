# Asteri

## Description

![Asteri Logo with Text](images/text_logo.png)

Asteri is a 2D side view platformer game. Player must complete stages that are full of traps and rhythmed by catchy music.

## Assets

Almost all assets in this game are self-made. Assets taken from external sources are in-game music. Music credits are given to [NoCopyrightSounds](https://www.youtube.com/channel/UC_aEa8K-EOJ3D6gOs7HcyNg).

## How to Play

The player controls the character by using the arrow keys on the keyboard. There are several environments and objects that player can interact with such as dashing points, gravity changers, moving and static platforms, portals, spikes, trampolines, and so on. If a player loses, the player can send his playing record to the game website using the GUI in the game. The record can be viewed on [Asteri Official Site](https://asteri-game.herokuapp.com).

## Screenshots

![Screenshot 1](images/screenshot1.png)

![Screenshot 2](images/screenshot2.png)

![Screenshot 3](images/screenshot3.png)

![Screenshot 4](images/screenshot4.png)

![Screenshot 5](images/screenshot5.png)

![Screenshot 6](images/screenshot6.png)

![Screenshot 7](images/screenshot7.png)

![Screenshot 8](images/screenshot8.png)
